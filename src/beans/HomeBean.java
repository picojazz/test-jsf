package beans;

import java.io.IOException;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import dao.ClientHome;
import dao.CompteHome;
import entities.Client;
import entities.Compte;

@ManagedBean
@SessionScoped
public class HomeBean {

	private Client cli ;
	private Compte c ;
	private String mode;
	private String montant;
	
	public HomeBean() {
		super();
		// TODO Auto-generated constructor stub
		Map<String, String> params = FacesContext.getCurrentInstance()
                .getExternalContext().getRequestParameterMap();
		System.out.println(params.get("id"));
		
		  cli = ClientHome.findById(Integer.parseInt(params.get("id"))); 
		  c = CompteHome.getCompteClient(Integer.parseInt(params.get("id")));
		 
		
	}
	public void valider() {
		FacesContext context = FacesContext.getCurrentInstance();
		
		int m = Integer.parseInt(montant);
		
		if(mode.equals("1")) {
			if(c.getSolde() < m) {
				context.addMessage(null, new FacesMessage("le montant saisi est inferieur a votre solde"));
			}else {
				c.setSolde(c.getSolde()-m);
				
				CompteHome.merge(c);
			}
			
		}
		if(mode.equals("2")) {
			System.out.println("in 2");
			
			c.setSolde(c.getSolde()+m);
			
			CompteHome.merge(c);
			
		}
	}
	
	public String getMontant() {
		return montant;
	}
	public void setMontant(String montant) {
		this.montant = montant;
	}
	public String getMode() {
		return mode;
	}


	public void setMode(String mode) {
		this.mode = mode;
	}


	public Client getCli() {
		return cli;
	}
	public void setCli(Client cli) {
		this.cli = cli;
	}
	public Compte getC() {
		return c;
	}
	public void setC(Compte c) {
		this.c = c;
	}
	
	public void logout() {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		/*
		 * FacesContext.getCurrentInstance().getExternalContext() .invalidateSession();
		 */
		System.out.println("logout");
	        
		try {
			ec.invalidateSession();
			ec.redirect("./login.jsf");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
