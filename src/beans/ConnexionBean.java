package beans;

import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import dao.ClientHome;
import entities.Client;

@ManagedBean
public class ConnexionBean {
	
	private Client client;

	public ConnexionBean() {
		super();
		// TODO Auto-generated constructor stub
		client = new Client();
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
	
	public void connexion() {
		FacesContext context = FacesContext.getCurrentInstance();
		
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		Client c = ClientHome.connexion(client.getLogin(), client.getPassword());
		if(c != null) {
			try {
				ec.redirect(ec.getRequestContextPath() + "/home.jsf?id=" + c.getIdClient());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else {
			context.addMessage(null, new FacesMessage("nom d'utilisateur ou mot de passe incorrect"));
		}
		
	}
	
	

}
