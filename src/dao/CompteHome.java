package dao;
// Generated 18 janv. 2020 14:27:02 by Hibernate Tools 4.0.1.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import entities.Client;
import entities.Compte;
import utils.SessionFactoryProvider;

import static org.hibernate.criterion.Example.create;

/**
 * Home object for domain model class Compte.
 * @see dao.Compte
 * @author Hibernate Tools
 */
public class CompteHome {

	private static final Log log = LogFactory.getLog(CompteHome.class);

	private final static SessionFactory sessionFactory = SessionFactoryProvider.getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}
	
	public static Compte getCompteClient(int idClient) {
		
		Session session=null;
		try {
			 session = sessionFactory.openSession();
				// create a new criteria
				Criteria crit = session.createCriteria(Compte.class);
				crit.add(Restrictions.eq("client.id", idClient));
				
				Object o=crit.uniqueResult();
			if (o == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return (Compte) o;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
		
	}

	public static void persist(Compte transientInstance) {
		log.debug("persisting Compte instance");
		try {
			sessionFactory.openSession().persist(transientInstance);
			
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Compte instance) {
		log.debug("attaching dirty Compte instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Compte instance) {
		log.debug("attaching clean Compte instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Compte persistentInstance) {
		log.debug("deleting Compte instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public static void merge(Compte detachedInstance) {
		log.debug("merging Compte instance");
		try {
			Session session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			 session.merge(detachedInstance);
			 tx.commit();
			log.debug("merge successful");
			
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Compte findById(int id) {
		log.debug("getting Compte instance with id: " + id);
		try {
			Compte instance = (Compte) sessionFactory.getCurrentSession().get("dao.Compte", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Compte> findByExample(Compte instance) {
		log.debug("finding Compte instance by example");
		try {
			List<Compte> results = (List<Compte>) sessionFactory.getCurrentSession().createCriteria("dao.Compte")
					.add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
