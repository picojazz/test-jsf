package dao;
// Generated 18 janv. 2020 14:27:02 by Hibernate Tools 4.0.1.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import entities.Client;

import utils.SessionFactoryProvider;

import static org.hibernate.criterion.Example.create;

/**
 * Home object for domain model class Client.
 * @see dao.Client
 * @author Hibernate Tools
 */
public class ClientHome {

	private static final Log log = LogFactory.getLog(ClientHome.class);

	private final static SessionFactory sessionFactory = SessionFactoryProvider.getSessionFactory();

	
	
	public static Client connexion(String login,String password) {
		Session session=null;
		if (null == login ) {
			throw new IllegalArgumentException("Login and password are mandatory. Null values are forbidden.");
		}		
		try {
			 session = sessionFactory.openSession();
			// create a new criteria
			Criteria crit = session.createCriteria(Client.class);
			crit.add(Restrictions.eq("login", login.trim()));
			crit.add(Restrictions.eq("password", password.trim()));
			Object o=crit.uniqueResult();
			if(o!=null) {
			Client user = (Client)o;//sous-classement
			//session.close();
			return user;
			}
			else{//session.close(); 
			return null;}
		}
		catch(Exception e) {
			e.printStackTrace();
			session.close();
			// Critical errors : database unreachable, etc.
			return null;
		}
		/*finally{
			session.close();
		}*/
	}

	public void persist(Client transientInstance) {
		log.debug("persisting Client instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Client instance) {
		log.debug("attaching dirty Client instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Client instance) {
		log.debug("attaching clean Client instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Client persistentInstance) {
		log.debug("deleting Client instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Client merge(Client detachedInstance) {
		log.debug("merging Client instance");
		try {
			Client result = (Client) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public static Client findById(int id) {
		log.debug("getting Client instance with id: " + id);
		Session session=null;
		try {
			 session = sessionFactory.openSession();
				// create a new criteria
				Criteria crit = session.createCriteria(Client.class);
				crit.add(Restrictions.eq("id", id));
				
				Object o=crit.uniqueResult();
			if (o == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return (Client) o;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Client> findByExample(Client instance) {
		log.debug("finding Client instance by example");
		try {
			List<Client> results = (List<Client>) sessionFactory.getCurrentSession().createCriteria("dao.Client")
					.add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
